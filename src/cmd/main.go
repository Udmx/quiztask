package main

import (
	"github.com/udmx/quiztask/api"
	"github.com/udmx/quiztask/config"
	"github.com/udmx/quiztask/data/cache"
)

func main() {
	cfg := config.GetConfig()
	cache.InitRedis(cfg)
	api.InitServer(cfg)
	defer cache.CloseRedis()
}
