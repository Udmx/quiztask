package service_errors

const (
	// User
	UsernameExists   = "Username exists"
	PermissionDenied = "Permission denied"

	// DB
	RecordNotFound = "Record not found"
)
