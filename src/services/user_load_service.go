package services

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v7"
	"github.com/udmx/quiztask/config"
	"github.com/udmx/quiztask/data/cache"
	"time"
)

type UserLoadService struct {
	cfg         *config.Config
	redisClient *redis.Client
}

func NewUserLoadService(cfg *config.Config) *UserLoadService {
	return &UserLoadService{
		cfg:         cfg,
		redisClient: cache.GetRedis(),
	}
}

// AddLoad Add a new load record for the given user ID
func (s *UserLoadService) AddLoad(c context.Context, userID string, loadTime string) error {
	loadsKey := fmt.Sprintf("loads:%s", userID)
	timestamp := time.Now().UTC()

	_, err := s.redisClient.XAdd(&redis.XAddArgs{
		Stream: loadsKey,
		Values: map[string]interface{}{
			"user_id":    userID,
			"timestamp":  timestamp.Format(time.RFC3339),
			"load_time":  loadTime,
			"api_called": "user_load",
		},
	}).Result()

	return err
}
