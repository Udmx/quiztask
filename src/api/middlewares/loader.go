package middlewares

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"time"
)

func LoadTimeMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()

		c.Next()

		loadTime := time.Since(start)
		c.Header("X-Load-Time", fmt.Sprintf("%.2fms", loadTime.Seconds()*1000))
	}
}
