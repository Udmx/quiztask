package handlers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/udmx/quiztask/api/helper"
	"github.com/udmx/quiztask/config"
	"github.com/udmx/quiztask/services"
	"net/http"
)

type UsersLoadHandler struct {
	service *services.UserLoadService
}

func NewUsersLoadHandler(cfg *config.Config) *UsersLoadHandler {
	return &UsersLoadHandler{
		service: services.NewUserLoadService(cfg),
	}
}

func (h *UsersLoadHandler) Handle(c *gin.Context) {
	userID := c.Param("id")

	go func() {
		loadTime := c.Writer.Header().Get("X-Load-Time")
		h.service.AddLoad(c, userID, loadTime)
		//TODO: set logger and see it on the kibana
		//if err != nil {
		//	c.AbortWithStatusJSON(helper.TranslateErrorToStatusCode(err),
		//		helper.GenerateBaseResponseWithError(nil, false, helper.InternalError, err),
		//	)
		//	return
		//}
	}()

	c.JSON(http.StatusCreated, helper.GenerateBaseResponse(fmt.Sprintf("Load added for user %s", userID), true, helper.Success))
}
