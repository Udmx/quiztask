package routers

import (
	"github.com/gin-gonic/gin"
	"github.com/udmx/quiztask/api/handlers"
	"github.com/udmx/quiztask/config"
)

func UserLoad(r *gin.RouterGroup, cfg *config.Config) {
	handler := handlers.NewUsersLoadHandler(cfg)

	r.GET("/:id/load", handler.Handle)
}
