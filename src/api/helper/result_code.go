package helper

type ResultCode int

const (
	Success        ResultCode = 0
	NotFoundError  ResultCode = 40401
	ForbiddenError ResultCode = 40301
	InternalError  ResultCode = 50002
)
