package api

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/udmx/quiztask/api/middlewares"
	routers "github.com/udmx/quiztask/api/routes"
	"github.com/udmx/quiztask/config"
)

//InitServer Initializes server.
func InitServer(cfg *config.Config) {
	r := gin.New()

	r.Use(gin.Logger(), gin.Recovery())
	r.Use(middlewares.LoadTimeMiddleware())
	r.Use(middlewares.Cors(cfg))

	api := r.Group("/api")
	v1 := api.Group("/v1")
	{
		users := v1.Group("/users")
		routers.UserLoad(users, cfg)
	}

	r.Run(fmt.Sprintf(":%s", cfg.Server.Port))
}
