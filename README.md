## How to run

### On docker:

### start

##### Step one(cd to docker directory):
```
docker compose -f "docker/docker-compose.yml" up -d --build
```
##### Step two (cd to cmd directory):
```
go run main.go
```

#### stop

```
docker compose --file 'docker/docker-compose.yml' --project-name 'docker' down
```


## The result

<p align="center"><img src='/docs/results/rrr.png' alt='Redis' /></p>